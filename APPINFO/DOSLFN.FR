Begin3
Language:    FR, 850, French (Standard)
Title:       DOSLFN
Description: Fournit l'API de noms de fichier longs (LFN)
Summary:     Fournit l'API de noms de fichier longs (LFN) sous DOS (sans Windows). Cette version nécessite SHSUCDX pour la prise en charge des CD-ROM.
Keywords:    Noms de fichier longs, lfn
End
